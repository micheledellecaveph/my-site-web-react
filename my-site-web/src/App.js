import './App.css';

function App() {
  return (
    <div className="App">
      <div className='Navbar'> 
        <a href="#" className="Navbar-button">Projects</a>
        <a href="#" className="Navbar-button">Contacts</a>
     </div>
      <header className="App-header">
        <h1 className="App-header__name">Michele Delle cave</h1>
        <p className="App-header__descriptions">I am a web developer at STEM5. My passion is building simple, beautiful user experiences.</p>
      </header>
    </div>
  );
}

export default App;
